#!/usr/bin/env python
# Copyright (c) 2017-2020 CERN
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#
# Author:
#  Belmiro Moreira <belmiro.moreira@cern.ch>

import argparse
import boto3
import ConfigParser
import datetime
import sys
import logging
import os
import rados
import rbd

from prettytable import PrettyTable
from sqlalchemy import and_
from sqlalchemy import or_
from sqlalchemy import MetaData
from sqlalchemy import select
from sqlalchemy import Table
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

logger = logging.getLogger('glance-backup')
logging.basicConfig(level=logging.ERROR, format='%(asctime)s:%(levelname)s:%(message)s')
logging.getLogger('glance-backup').setLevel(logging.DEBUG)

class GlanceClient():
    def __init__(self, endpoints):
        engine = create_engine(endpoints['glance']['database_url'])
        engine.connect()
        Session = sessionmaker(bind=engine)
        db_session = Session()
        db_metadata = MetaData()
        db_metadata.bind = engine
        db_base = declarative_base()
        self.metadata = db_metadata
        self.stage_days = endpoints['backup']['stage_days']

    def get_images(self, image_id):
        images = Table('images', self.metadata, autoload=True)
        if image_id:
            available_images = select(columns=[images.c.id],
                whereclause=and_(images.c.id == image_id,
                images.c.status == 'active',
                images.c.deleted == False)).execute().fetchall()
        else:
            available_images = select(columns=[images.c.id],
                whereclause=and_(images.c.status != 'deleted',
                images.c.status != 'killed',
                images.c.status != 'queued',
                images.c.size < '300000000000',
                images.c.size > '0',
                images.c.disk_format != 'vhd',
                ~images.c.name.like('rally-%'),
                images.c.deleted == False)).execute().fetchall()
        return available_images

    def get_images_to_clean(self):
        images = Table('images', self.metadata, autoload=True)
        now = datetime.datetime.utcnow()
        delete_date = now - datetime.timedelta(days=int(self.stage_days))
        valid_images = select(columns=[images.c.id],
            whereclause=and_(images.c.deleted == True,
            images.c.deleted_at < delete_date)).execute().fetchall()
        return valid_images

    def get_image_info(self, image_id):
        images = Table('images', self.metadata, autoload=True)
        if image_id:
            available_images = select(columns=[images.c.id, images.c.created_at,
                images.c.deleted_at, images.c.owner, images.c.checksum, images.c.size, images.c.name],
                whereclause=(images.c.id == image_id)).execute().fetchall()
        else:
            now = datetime.datetime.utcnow()
            delete_date = now - datetime.timedelta(days=int(self.stage_days))
            available_images = select(columns=[images.c.id, images.c.created_at,
                images.c.deleted_at, images.c.owner, images.c.checksum, images.c.size],
                whereclause=and_(images.c.checksum != None,
                ~images.c.name.like('rally-%'),
                images.c.disk_format != 'vhd',
                images.c.size > '0',
                images.c.status != 'queued',
                images.c.status != 'killed',
                or_(images.c.deleted_at > delete_date, images.c.deleted == False))).execute().fetchall()
        return available_images

class S3Client():
    def __init__(self, endpoints):
        self.access_key = endpoints['aws']['access_key']
        self.secret_key = endpoints['aws']['secret_key']
        self.endpoint = endpoints['aws']['endpoint']
        self.bucket = endpoints['aws']['bucket']

        session = boto3.Session(aws_access_key_id=self.access_key,
            aws_secret_access_key=self.secret_key)
        s3_resource = session.resource('s3', endpoint_url=self.endpoint)
        self.s3_client = session.client('s3', endpoint_url=self.endpoint)

    def backup_exists(self, key):
        try:
            self.s3_client.list_objects(Bucket=self.bucket, Prefix=key)['Contents']
        except:
            return False
        return True

    def upload_backup(self, filename, key):
        try:
            self.s3_client.upload_file(filename, self.bucket, key)
        except:
            logger.error("Can't upload image to S3: %s, key")
            return -1

    def download_backup(self, key):
        try:
            self.s3_client.download_file(self.bucket, key, key)
        except:
            logger.error("Can't download image from S3: %s", key)
            return -1

    def delete_backup(self, key):
        try:
            self.s3_client.delete_object(Bucket=self.bucket, Key=key)
        except:
            logger.error("Can't delete image from S3: %s", key)
            return -1

    def get_objects(self):
        kwargs = {'Bucket': self.bucket}
        s3_objects = []
        while True:
            resp = self.s3_client.list_objects(**kwargs)
            for obj in resp['Contents']:
                s3_objects.append(obj['Key'])
            try:
                kwargs['Marker'] = resp['NextMarker']
            except KeyError:
                break

        return s3_objects


class RBDImageIterator():
    def __init__(self, endpoints, name):
        self.name = name
        self.chunk_size = 33554432
        self.endpoints = endpoints

    def __iter__(self):
        try:
            with rados.Rados(conffile=self.endpoints['rbd']['configfile'], rados_id=self.endpoints['rbd']['pool']) as conn:
                with conn.open_ioctx(self.endpoints['rbd']['pool']) as ioctx:
                    with rbd.Image(ioctx, self.name) as image:
                        size = image.size()
                        bytes_left = size
                        while bytes_left > 0:
                            length = min(self.chunk_size, bytes_left)
                            data = image.read(size - bytes_left, length)
                            bytes_left -= len(data)
                            yield data
                        raise StopIteration()
        except Exception as e:
#            logger.error('Error downloading image: %s', self.name)
            pass


def image_backup(endpoints, images_id):
    glance_session = GlanceClient(endpoints)
    s3_session = S3Client(endpoints)
    if images_id:
        for image_id in images_id:
            if glance_session.get_images(image_id):
                __image_backup(endpoints, image_id, s3_session)
            else:
                logger.error("Image not available in Glance: %s", image_id)
    else:
        images_id = glance_session.get_images(None)
        for (image_id,) in images_id:
            __image_backup(endpoints, image_id, s3_session)


def __image_backup(endpoints, image_id, s3_session):
    if not s3_session.backup_exists(image_id):
        if os.path.exists(endpoints['tempfile']['path']+'tmp.img'):
            try:
                os.remove(endpoints['tempfile']['path']+'tmp.img')
            except:
                logger.error("Can't remove temporary local image file")
                return -1
        try:
            image_data = RBDImageIterator(endpoints, image_id)
            logger.info("Downloading image from RBD: %s", image_id)
            with open(endpoints['tempfile']['path']+'tmp.img', 'wb') as f:
                for i in image_data:
                    f.write(i)
        except:
            logger.error("Can't download image from RBD: %s", image_id)
            return -1
        try:
            if os.path.getsize(endpoints['tempfile']['path']+'tmp.img') == 0:
                logger.info("Image size is 0. Not uploading to S3: %s", image_id)
                return -1
            logger.info("Uploading image to S3: %s", image_id)
            s3_session.upload_backup(endpoints['tempfile']['path']+'tmp.img', image_id)
        except:
            logger.error("Can't upload image to S3: %s", image_id)
            return -1
    else:
            logger.info("Image already exists in S3: %s", image_id)


def image_recover(endpoints, images_id):
    s3_session = S3Client(endpoints)
    if images_id:
        for image_id in images_id:
            if s3_session.backup_exists(image_id):
                s3_session.download_backup(str(image_id))
            else:
                logger.error("Backup doesn't exist in S3: %s", image_id)
    else:
        logger.error("Image_ID required to recover backup from S3")


def image_clean(endpoints, images_id):
    glance_session = GlanceClient(endpoints)
    s3_session = S3Client(endpoints)
    if images_id:
        for image_id in images_id:
            if s3_session.backup_exists(image_id):
                s3_session.delete_backup(image_id)
            else:
                logger.error("Image backup doesn't exist in S3: %s", image_id)
    else:
        all_deleted_images_set = set()
        all_objects_set = set()

        all_deleted_images = glance_session.get_images_to_clean()
        objects = s3_session.get_objects()
        for (image_id,) in all_deleted_images:
            all_deleted_images_set.add(image_id)
        for obj in objects:
            all_objects_set.add(obj)

        objects_to_delete = all_objects_set & all_deleted_images_set

        print len(all_objects_set)
        print len(all_deleted_images_set)
        print len(objects_to_delete)

        for image_id in objects_to_delete:
            s3_session.delete_backup(image_id)
            image = glance_session.get_image_info(image_id)
            [(i_id, created_at, deleted_at, owner, checksum, size, name)] = image
            logger.info("Deleting image from S3: %s %s %s %s" % (i_id, created_at, deleted_at, name))


def image_info(endpoints, images_id):
    glance_session = GlanceClient(endpoints)
    s3_session = S3Client(endpoints)
    ptable = PrettyTable(["image_id", "created_at", "deleted_at", "owner",
        "checksum", "size", "backup_on_s3"])
    if images_id:
        for image_id in images_id:
            image = glance_session.get_image_info(image_id)
            if image:
                [(image_id, created_at, deleted_at, owner, checksum, size)] = image
                if s3_session.backup_exists(image_id):
                    backup = '\033[1m\033[92mavailable\033[0m'
                else:
                    backup = '\033[1m\033[91mNOT available\033[0m'

                ptable.add_row([image_id, created_at, deleted_at, owner, checksum, size, backup])
    else:
        images = glance_session.get_image_info(None)
        for (image_id, created_at, deleted_at, owner, checksum, size) in images:
            if s3_session.backup_exists(image_id):
                backup = '\033[1m\033[92mavailable\033[0m'
            else:
                backup = '\033[1m\033[91mNOT available\033[0m'
            ptable.add_row([image_id, created_at, deleted_at, owner, checksum, size, backup])

    print ptable

def get_configuration(config_file):
    parser = ConfigParser.SafeConfigParser()
    parser.read(config_file)
    endpoints = {}
    endpoints['aws'] = {}
    endpoints['rbd'] = {}
    endpoints['glance'] = {}
    endpoints['backup'] = {}
    endpoints['tempfile'] = {}

    try:
        endpoints['aws']['access_key'] = parser.get('aws', 'access_key')
        endpoints['aws']['secret_key'] = parser.get('aws', 'secret_key')
        endpoints['aws']['endpoint'] = parser.get('aws', 'endpoint_url')
        endpoints['aws']['bucket'] = parser.get('aws', 'bucket')
        endpoints['rbd']['pool'] = parser.get('rbd', 'pool')
        endpoints['rbd']['configfile'] = parser.get('rbd', 'configfile')
        endpoints['glance']['database_url'] = parser.get('glance', 'database_url')
        endpoints['backup']['stage_days'] = parser.get('backup', 'stage_days')
        endpoints['tempfile']['path'] = parser.get('tempfile', 'path')
    except Exception as e:
        logger.error("Can't parse configuration file")
        print e
        sys.exit(1)
    return endpoints

def parse_cmdline_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--image-id",
        action="append",
        help="Image ID")
    parser.add_argument('action',
        choices=['backup', 'recover', 'clean', 'info'],
        help="Action to be performed")
    parser.add_argument("--config",
        default='/etc/glance-backup/config.conf',
        help='configuration file')
    return parser.parse_args()

def main():
    try:
        args = parse_cmdline_args()
    except Exception as e:
        logger.error("Wrong arguments")
        sys.exit(1)

    endpoints = get_configuration(args.config)

    if args.action == 'backup':
        image_backup(endpoints, args.image_id)
    elif args.action == 'recover':
        image_recover(endpoints, args.image_id)
    elif args.action == 'clean':
        image_clean(endpoints, args.image_id)
    elif args.action == 'info':
        image_info(endpoints, args.image_id)


if __name__ == "__main__":
    main()